# Because powershell is terrible and doesn't send exit codes properly
$ErrorActionPreference = "Stop"

Write-Host "Importing database-factory module..."
Import-Module ./test-database-factory.psm1

Write-Host "Importing SQLPS module..."
Import-Module SqlServer -DisableNameChecking;

$tedh =  New-TedHDatabase -databaseName "TEDH_ksQaa"
$tedhcf = New-TedHCFDatabase -databaseName "TEDH_ksQaa_CustomForms"
$tedp = New-TedPDatabase -databaseName "TEDP_ksQaa"
$tedpcf = New-TedPCFDatabase -databaseName "TEDP_ksQaa_CustomForms"

$qaTestDatabases = (
  "TEDH_ksQaa",
  "TEDH_ksQaa_CustomForms",
  "TEDP_ksQaa",
  "TEDP_ksQaa_CustomForms"
)

$databaseObjects = (
  $tedh,
  $tedhcf,
  $tedp,
  $tedpcf
)

ForEach($database in $qaTestDatabases) {
  Write-Host "Removing database $database..."
  Try {
    Write-Host "Setting database to single user mode"
    Invoke-Sqlcmd -ServerInstance "localhost" -Username $env:SQL_USERNAME -Password $env:SQL_PASSWORD -Query "alter database $database set single_user with rollback immediate"
  }
  Catch {
    Write-Host "$database does not exist"
  }
  Invoke-Sqlcmd -ServerInstance "localhost" -Username $env:SQL_USERNAME -Password $env:SQL_PASSWORD -Query "drop database if exists $database"
}

ForEach($dbo in $databaseObjects) {
  Write-Host "Restoring backup for $($dbo.DatabaseName)"
  Start-Job -Name "$($dbo.DatabaseName) Restore" -FilePath "test-restore-database.ps1" -ArgumentList $dbo
}

# Wait for all background jobs to complete
Get-Job | Wait-Job

# Get all job results
Get-Job | Receive-Job

Write-Host "All databases successfully downloaded."
