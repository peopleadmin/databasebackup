﻿using System;
using System.Data.SqlClient;
using System.Runtime.InteropServices;
using System.Security.Cryptography;
using System.Text;

namespace DatabaseBackup
{
    class Program
    {
        //Stops program from idling
        [DllImport("kernel32.dll", CharSet = CharSet.Auto, SetLastError = true)]
        static extern EXECUTION_STATE SetThreadExecutionState(EXECUTION_STATE esFlags);

        [FlagsAttribute]
        public enum EXECUTION_STATE : uint
        {
            ES_AWAYMODE_REQUIRED = 0x00000040,
            ES_CONTINUOUS = 0x80000000,
            ES_DISPLAY_REQUIRED = 0x00000002,
            ES_SYSTEM_REQUIRED = 0x00000001
        }
        static void ProcessResume_Linq()
        {
            // ...
        }

        void restoreDatabases()
        {
            var ps1File = @"C:\DatabaseBackup\test-restore-regression-databases.ps1";
            var startInfo = new ProcessStartInfo()
            {
                FileName = "powershell.exe",
                Arguments = $"-NoProfile -ExecutionPolicy unrestricted -file \"{ps1File}\"",
                UseShellExecute = false
            };
            Process.Start(startInfo);
        }

        static void Main(string[] args)
        {
            try
            {
                Console.WriteLine("-------------------------------");
                Console.WriteLine("Connecting to OnDemand database to retrieve deployments\r");
                Console.WriteLine("-------------------------------");

                //Maybe add another connection string in config file like how we did for OnDemand on the next line
                SqlConnection HireDBConnection = new SqlConnection();
                SqlConnection OnDemandConnection = new SqlConnection(GeneratePasswordSaltAndHash.Properties.Settings.Default.ondemand_connectionstring);


                OnDemandConnection.Open();
                // modify SQL command on line 43. Want all product deployments not just hire. Get SQL command from Joe
                //finds the database
                SqlDataReader ClientDeploymentReader = new SqlCommand("select HostingURL, cp.Value As HireDBConnection from ClientDeployment cd JOIN ClientPreferences cp ON cd.ClientDeploymentID = cp.ClientDeploymentID where cd.ProductID = 2 and cp.KeyName = 'SYSTEM_DBCONNECTION' and ISNULL(Deactivated,'') <> 'T'", OnDemandConnection).ExecuteReader();

                if (ClientDeploymentReader.HasRows)
                {
                    while (ClientDeploymentReader.Read())
                    {
                        Console.WriteLine("-------------------------------");
                        Console.WriteLine("Now Attempting to get info for each Hire Database...");
                        Console.WriteLine("Database:{0} ", ClientDeploymentReader.GetString(1));
                        Console.WriteLine("-------------------------------");

                        SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder();

                        //Connect to each Hire instance
                        HireDBConnection = new SqlConnection(ClientDeploymentReader.GetString(1));

                        try
                        {
                            HireDBConnection.Open();

                            //NEED TO GET HIREDBCONNECTION
                            SqlDataReader ApplicantProfileHeaderReader = new SqlCommand("select Count(ApplicantProfileID) from ApplicantProfile_Header Where [Password] IS NOT NULL and Password_Hash IS NULL", HireDBConnection).ExecuteReader();

                            ApplicantProfileHeaderReader.Read();
                            int iterationCount = (int)Math.Floor((double)(ApplicantProfileHeaderReader.GetInt32(0) / 500) + 1);


                            Console.WriteLine("-------------------------------");
                            Console.WriteLine("ApplicantCount:{0} Iteration Count:{1} ", ApplicantProfileHeaderReader.GetInt32(0), iterationCount);
                            Console.WriteLine("-------------------------------");
                            ApplicantProfileHeaderReader.Close();

                            //Loop through each iteration and create the salt and hash
                            for (int i = 0; i < iterationCount; i++)
                            {
                                // Prevent Idling
                                SetThreadExecutionState(EXECUTION_STATE.ES_CONTINUOUS | EXECUTION_STATE.ES_SYSTEM_REQUIRED | EXECUTION_STATE.ES_AWAYMODE_REQUIRED);

                                Console.WriteLine("------------------------------------------");
                                Console.WriteLine("Executing batch:{0} of {1} for Client: {2} ", i, iterationCount, ClientDeploymentReader.GetString(1));
                                Console.WriteLine("------------------------------------------");
                                SqlDataReader ApplicantProfileHeaderReader2 = new SqlCommand("select top(500) ApplicantProfileID, [Password] from ApplicantProfile_Header Where [Password] IS NOT NULL and Password_Hash IS NULL", HireDBConnection).ExecuteReader();

                                ApplicantProfileHeaderReader2.Close();
                            }

                            //check to see if there are still applicants left
                            ApplicantProfileHeaderReader = new SqlCommand("select Count(ApplicantProfileID) from ApplicantProfile_Header Where [Password] IS NOT NULL and Password_Hash IS NULL", HireDBConnection).ExecuteReader();

                            ApplicantProfileHeaderReader.Read();
                            if (ApplicantProfileHeaderReader.HasRows)
                            {
                                Console.WriteLine("------------------------------------------");
                                Console.WriteLine("Applicants still remaining to be hashed:{0}", ApplicantProfileHeaderReader.GetInt32(0));
                                Console.WriteLine("------------------------------------------");
                            }
                            HireDBConnection.Close();
                        }
                        catch (SqlException ex)
                        {
                            string errorMessage = ex.Message;
                            Console.WriteLine(errorMessage);
                        }
                    }
                }
                else
                {
                    Console.WriteLine("No Deployments found.");
                }

                // Allow Idling
                SetThreadExecutionState(EXECUTION_STATE.ES_CONTINUOUS);

                OnDemandConnection.Close();
                Console.Write("Press any key to close.");
                Console.ReadKey();
            }

            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

    }
}